'use strict'

const menuButton = document.getElementById("js-mobile-menu");
const headerClass = document.querySelector(".header-mobile");
const headerMainClass = document.querySelector(".header");
const headerContent = document.querySelector(".header__list");


menuButton.addEventListener("click", function() {
    headerClass.classList.toggle("is-active");
    headerMainClass.classList.toggle("menu-open");
    headerContent.classList.toggle("is-open");
    document.body.classList.toggle("is-locked");
    headerMainClass.classList.remove("submenu-open");
});

if (document.querySelector("[data-video-item]")) {
    document.querySelector("[data-video-button]").addEventListener('click',function(e){
        e.preventDefault();
        let wrapper = document.querySelector("[data-video-item]");
        wrapper.classList.add("video-play");
        videoPlay(wrapper);
    });
}

function videoPlay(wrapper) {
    const video = wrapper.querySelector("video");
    const iframe = wrapper.querySelector("iframe");
    if (video) {
        const src = video.dataset.video;
        video.setAttribute('src', src);
    }
    else {
        const src = iframe.dataset.video;
        iframe.setAttribute('src', src);
    }
}

const sliderClass = document.querySelector(".slider");

if (sliderClass) {
    $('.js-slider').each(function(){
        $(this).slick({
            infinite: false,
            slidesToShow: 1.3,
            slidesToScroll: 1,
            arrows: false  
        });
      });
}

var headerScroll = document.querySelector('header');

if (headerScroll) {
    var scrollPosition = window.scrollY;

    window.addEventListener('scroll', function() {

        scrollPosition = window.scrollY;
    
        if (scrollPosition > 0) {
            headerScroll.classList.add('header--scrolled');
        } 
        
        else {
            headerScroll.classList.remove('header--scrolled');
        }
    
    });
}